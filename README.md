## Gladius

![Gladius](/logo/logo.png)

### Introducción
Hola, soy **Tomas Carceglia**, un programador y Game Designer Argentino. Trabajo con C++, Raylib y Actionscript principalmente.
El juego es una version romana y cuerpo a cuerpo del famoso Gradius, inspirado por el arma de epoca de similar nombre Gladius.

### Screenshots
En el futuro cargamos aca todos los screenshots

### Redes Sociales

**Twitter:** https://twitter.com/KryEdge  
**Itch.io:** https://itch.io/profile/kryedge

#include "gameplay.h"

#include "raylib.h"

#include "main.h"
#include "draw.h"


namespace gladius
{	
	extern float const radioPlayer = 30.0f;
	extern int const maxBullets = 30;

	enum class state
	{
		game, menu, gameover, creditos, winscreen
	}; 
	state currentState; 
	int estado;

	Bullet magazine[maxBullets];
	float timer = 0.0f;
	Vector2 posEne = { 600, 200 };
	Rectangle Enemy { posEne.x,posEne.y, 30,30 };
	Vector2 posP1 = { (float)100, (float)screenHeight / 2 };

	void recoverBullets()
	{
		for (int i = 0; i < maxBullets; i++)
		{
			if (magazine[i].x > screenWidth + 50)
			{
				magazine[i].on = false;
			}
		}
	}
	void movBullets()
	{
		for (int i = 0; i < maxBullets; i++)
		{
			magazine[i].x += magazine[i].speed;
		}
	}
	void shoot()
	{		
		for (int i = 0; i < maxBullets; i++)
		{
			if (timer > 5)
			{
				magazine[i].on = true;
				timer = 0.0f;
			}
			else
			{
				timer += GetFrameTime();
			}
			if (!magazine[i].on)
			{
				magazine[i].x = posP1.x;
				magazine[i].y = posP1.y;
			}
		}
	}

	void playerMovement()
	{
		if (IsKeyDown(KEY_W) && (posP1.y - radioPlayer) > 0)
		{
			posP1.y -= 10;
		}
		else if (IsKeyDown(KEY_S) && (posP1.y + radioPlayer) < screenHeight)
		{
			posP1.y += 10;
		}
	}

	void enemyMovement()
	{
		if (Enemy.x > 0)
		{
			Enemy.x -= 10;
		}
		else
		{
			Enemy.x = static_cast<float>(screenWidth + 20);
			Enemy.y = static_cast<float>(GetRandomValue(40, screenHeight));
		}
	}

	void gameplay()
	{
		estado = static_cast<int>(currentState);

		switch (currentState)
		{
		case gladius::state::game:
			playerMovement();
			enemyMovement();			
			if (CheckCollisionCircleRec(posP1,radioPlayer,Enemy))
			{
				currentState = gladius::state::gameover;
			}
			for (int i = 0; i < maxBullets; i++)
			{
				Vector2 posBull = { magazine[i].x,magazine[i].y };
				if (CheckCollisionCircleRec(posBull, 10, Enemy))
				{
					Enemy.y = screenWidth + 20;
				}
			}
			if (IsKeyPressed(KEY_P))
			{
				currentState = gladius::state::menu;
			}
			if (IsKeyDown(KEY_J))
			{
				shoot();
			}
			movBullets();
			recoverBullets();
			break;

		case gladius::state::menu:
			if (IsKeyPressed(KEY_O))
			{
				currentState = gladius::state::game;
			}
			break;

		case gladius::state::gameover:
			if (IsKeyDown(KEY_O))
			{
				currentState = gladius::state::game;
			}
			break;

		case gladius::state::creditos:
			if (IsKeyDown(KEY_O))
			{
				currentState = gladius::state::game;
			}
			break;
		case gladius::state::winscreen:

			if (IsKeyDown(KEY_O))
			{
				currentState = gladius::state::game;
			}
			break;

		default:
			break;
		}
		
	}

}

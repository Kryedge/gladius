#pragma once
#include "raylib.h"

namespace gladius
{

	extern float const radioPlayer;
	extern int const maxBullets;

	extern Vector2 posP1;
	extern Vector2 posEne;
	struct Bullet
	{	
		float x = -10.0f;
		float y = -10.0f;
		float speed = 15.0f;
		bool on = false;
	};
	extern Bullet magazine[];
	extern int estado;
	extern Rectangle Enemy;
	/*enum state { game, menu, gameover, creditos, winscreen };
	extern state currentState;*/

	void gameplay();
}

#include "raylib.h"

#include "gameplay.h"
#include "draw.h"

using namespace gladius;
extern const int screenWidth = 800;
extern const int screenHeight = 450;

int main(void)
{
	SetTargetFPS(60);
	InitWindow(screenWidth, screenHeight, "Gladius");

	while (!WindowShouldClose())
	{
		drawGame();
		gameplay();
	}


	CloseWindow();
	return 0;
}

//"Xcopy $(SolutionDir)Assets $(OutDir)Assets /y" ME TIRABA ERROR CON ESTO SOLO EN DEBUG
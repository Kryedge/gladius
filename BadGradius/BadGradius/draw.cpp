#include "draw.h"

#include "raylib.h"

#include "gameplay.h"


namespace gladius
{
	Texture2D mainmenu;
	Texture2D background1;
	Texture2D background2;
	Vector2 posBG1 = { 0,0 };
	Vector2 posBG2 = { posBG1.x + background1.width,0 };
	bool boot = true;
	
	void loadTextures()
	{
		mainmenu = LoadTexture("Assets/final/Gladius.png");
		background1 = LoadTexture("Assets/final/backG.png");
		background2 = LoadTexture("Assets/final/backG.png");
	}

	void unTextures()
	{
		UnloadTexture(mainmenu);
		UnloadTexture(background1);
		UnloadTexture(background2);
	}

	void moveBackground()
	{
		posBG1.x -= 10;
	}

	void drawGameplay()
	{
		DrawTexture(background1, static_cast<int>(posBG1.x), static_cast<int>(posBG1.y), WHITE);
		DrawTexture(background2, static_cast<int>(posBG2.x), static_cast<int>(posBG2.y), WHITE);
		for (int i = 0; i < maxBullets; i++)
		{
			DrawCircle(static_cast<int>(magazine[i].x), static_cast<int>(magazine[i].y), 10, YELLOW);
		}
		DrawCircle(static_cast<int>(posP1.x), static_cast<int>(posP1.y), radioPlayer, BLACK);
		DrawRectangle(static_cast<int>(Enemy.x), static_cast<int>(Enemy.y), static_cast<int>(Enemy.width), static_cast<int>(Enemy.height), WHITE);
	}

	void drawMenu()
	{
		DrawTexture(mainmenu, 270, 20, WHITE);
	}

	void drawGameOver()
	{
		DrawText("MORISTE FLACO", 250, 220, 40, RED);
	}

	void drawGame()
	{
		if (boot)
		{
			loadTextures();
			mainmenu.height = 250;
			mainmenu.width = 250;
			boot = false;
		}

		BeginDrawing();
		ClearBackground(BLACK);
		
		switch (estado)
		{
		case 0:
			moveBackground();
			drawGameplay();
			break;
		case 1:
			drawMenu();
			break;
		case 2:
			drawGameOver();
			break;
		case 3:
			
			break;
		case 4:
			
			break;
		default:
			break;
		}

		EndDrawing();

	}
}
